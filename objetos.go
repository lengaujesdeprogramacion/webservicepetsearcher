package main

type Pet struct {
	Pet_id       string `json:"pet_id"`
	Especie      string `json:"especie"`
	Raza         string `json:"raza"`
	Nombre       string `json:"nombre"`
	Descripcion  string `json:"descripcion"`
	User_id      string `json:"user_id"`
	Recompensa   string `json:"recompensa"`
	Foto         string `json:"foto"`
	Localizacion string `json:"localizacion"`
}

type User struct {
	User_id      string `json:"user_id"`
	First_name   string `json:"first_name"`
	Last_name    string `json:"last_name"`
	Phone_number string `json:"phone_number"`
	City         string `json:"city"`
	Country      string `json:"country"`
	Usuario      string `json:"usuario"`
	Contrasena   string `json:"contrasena"`
}

type Anuncio struct {
	Anuncio_id  string `json:"anuncio_id"`
	Titulo      string `json:"titulo" `
	Descripcion string `json:"descripcion"`
	Categoria   string `json:"categoria"`
	User_id     string `json:"user_id"`
}
