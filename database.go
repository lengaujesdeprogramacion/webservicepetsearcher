package main

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

type Store interface {
	CreatePet(pet *Pet) error
	GetPets() ([]*Pet, error)
	GetPet(pet *Pet) (*Pet, error)
	DeletePet(pet *Pet) error
	CreateUser(user *User) error
	DeleteUser(user *User) error
	GetUser(user *User) (*User, error)
	GetUsers() ([]*User, error)
	GetUserById(user *User) (*User, error)
	CreateAnuncio(anuncio *Anuncio) error
	GetAnuncios() ([]*Anuncio, error)
	GetAnuncio(anuncios *Anuncio) (*Anuncio, error)
	GetAnuncioCategoria(anuncios *Anuncio) ([]*Anuncio, error)
}

type dbStore struct {
	db *sql.DB
}

func (store *dbStore) CreateAnuncio(anuncio *Anuncio) error {
	_, err := store.db.Query("INSERT INTO anuncios(anuncio_id, titulo, descripcion,categoria,user_id) VALUES ($1,$2,$3,$4,$5)", anuncio.Anuncio_id, anuncio.Titulo, anuncio.Descripcion, anuncio.Categoria, anuncio.User_id)
	return err
}

func (store *dbStore) GetAnuncios() ([]*Anuncio, error) {
	rows, err := store.db.Query("SELECT anuncio_id, titulo, descripcion,categoria,user_id from anuncios")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	anuncios := []*Anuncio{}
	for rows.Next() {
		anuncio := &Anuncio{}
		if err := rows.Scan(&anuncio.Anuncio_id, &anuncio.Titulo, &anuncio.Descripcion, &anuncio.Categoria, &anuncio.User_id); err != nil {
			return nil, err
		}
		anuncios = append(anuncios, anuncio)
	}
	return anuncios, nil
}

func (store *dbStore) GetAnuncio(anuncio *Anuncio) (*Anuncio, error) {
	rows, err1 := store.db.Query("SELECT anuncio_id, titulo, descripcion,categoria,user_id from anuncios where anuncio_id=$1", anuncio.Anuncio_id)
	if err1 != nil {
		return nil, err1
	}
	defer rows.Close()

	anuncios := []*Anuncio{}
	for rows.Next() {
		anuncio := &Anuncio{}
		if err := rows.Scan(&anuncio.Anuncio_id, &anuncio.Titulo, &anuncio.Descripcion, &anuncio.Categoria, &anuncio.User_id); err != nil {
			return nil, err
		}
		anuncios = append(anuncios, anuncio)
	}
	return anuncios[0], nil
}

func (store *dbStore) GetAnuncioCategoria(anuncio *Anuncio) ([]*Anuncio, error) {
	rows, err1 := store.db.Query("SELECT anuncio_id, titulo, descripcion,categoria,user_id from anuncios where categoria=$1", anuncio.Categoria)
	if err1 != nil {
		return nil, err1
	}
	defer rows.Close()

	anuncios := []*Anuncio{}
	for rows.Next() {
		anuncio := &Anuncio{}
		if err := rows.Scan(&anuncio.Anuncio_id, &anuncio.Titulo, &anuncio.Descripcion, &anuncio.Categoria, &anuncio.User_id); err != nil {
			return nil, err
		}
		anuncios = append(anuncios, anuncio)
	}
	return anuncios, nil
}

func (store *dbStore) CreatePet(pet *Pet) error {
	_, err := store.db.Query("INSERT INTO pets(nombre,descripcion,localizacion,especie,raza,recompensa,user_id) VALUES ($1,$2,$3,$4,$5,$6,$7,$8)", pet.Nombre, pet.Descripcion, pet.Localizacion, pet.Especie, pet.Raza, pet.Recompensa, pet.User_id)
	return err
}

func (store *dbStore) GetPets() ([]*Pet, error) {

	rows, err := store.db.Query("SELECT pet_id, especie,raza,nombre,localizacion,descripcion,user_id,recompensa,foto from pets")
	if err != nil {
		fmt.Print(err)
		return nil, err
	}
	defer rows.Close()
	pets := []*Pet{}
	for rows.Next() {
		pet := &Pet{}
		if err := rows.Scan(&pet.Pet_id, &pet.Especie, &pet.Raza, &pet.Nombre, &pet.Localizacion, &pet.Descripcion, &pet.User_id, &pet.Recompensa, &pet.Foto); err != nil {
			return nil, err
		}

		pets = append(pets, pet)

	}
	return pets, nil
}

func (store *dbStore) GetUsers() ([]*User, error) {

	rows, err := store.db.Query("SELECT user_id,nombre, apellido,telefono,city,country,usuario,contrasena from users")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users := []*User{}
	for rows.Next() {
		user := &User{}
		if err := rows.Scan(&user.User_id, &user.First_name, &user.Last_name, &user.Phone_number, &user.City, &user.Country, &user.Usuario, &user.Contrasena); err != nil {
			return nil, err
		}
		fmt.Printf(user.First_name)
		users = append(users, user)
	}
	return users, nil
}

func (store *dbStore) GetPet(pet *Pet) (*Pet, error) {
	rows, err1 := store.db.Query("SELECT pet_id, especie,raza,nombre,localizacion,descripcion,user_id,recompensa from pets where pet_id=$1", pet.Pet_id)
	if err1 != nil {
		fmt.Print(err1)
		return nil, err1
	}
	defer rows.Close()

	pets := []*Pet{}
	for rows.Next() {
		pet := &Pet{}
		if err := rows.Scan(&pet.Pet_id, &pet.Especie, &pet.Raza, &pet.Nombre, &pet.Localizacion, &pet.Descripcion, &pet.User_id, &pet.Recompensa); err != nil {
			return nil, err
		}
		pets = append(pets, pet)
	}
	return pets[0], nil
}

func (store *dbStore) GetUser(user *User) (*User, error) {
	rows, err := store.db.Query("SELECT user_id, nombre,apellido,telefono,city,country,usuario,contrasena from users where usuario=$1 AND contrasena=$2", user.Usuario, user.Contrasena)
	if err != nil {
		fmt.Print(err)
		return nil, err
	}
	defer rows.Close()

	users := []*User{}
	for rows.Next() {
		user := &User{}
		if err := rows.Scan(&user.User_id, &user.First_name, &user.Last_name, &user.Phone_number, &user.City, &user.Country, &user.Usuario, &user.Contrasena); err != nil {
			return nil, err
		}
		users = append(users, user)
	}
	return users[0], nil
}

func (store *dbStore) GetUserById(user *User) (*User, error) {
	rows, err := store.db.Query("SELECT user_id, nombre,apellido,telefono,city,country,usuario,contrasena from users where user_id=$1", user.User_id)
	if err != nil {
		fmt.Print(err)
		return nil, err
	}
	defer rows.Close()

	users := []*User{}
	for rows.Next() {
		user := &User{}
		if err := rows.Scan(&user.User_id, &user.First_name, &user.Last_name, &user.Phone_number, &user.City, &user.Country, &user.Usuario, &user.Contrasena); err != nil {
			return nil, err
		}
		users = append(users, user)
	}
	return users[0], nil
}

func (store *dbStore) DeletePet(pet *Pet) error {
	_, err := store.db.Query("DELETE FROM pets WHERE pet_id= $1)", pet.Pet_id)
	return err
}

func (store *dbStore) CreateUser(user *User) error {
	_, err := store.db.Query("INSERT INTO users(first_name, last_name, phone_number, city, country,usuario,contrasena) VALUES ($1,$2,$3,$4,$5)", user.First_name, user.Last_name, user.Phone_number, user.City, user.Country, user.Usuario, user.Contrasena)
	return err
}

func (store *dbStore) DeleteUser(user *User) error {
	_, err := store.db.Query("DELETE FROM user WHERE user_id= $1)", user.User_id)
	return err
}

var store Store

func InitStore(s Store) {
	store = s
}

func InicializarBD() {
	connString := "user=postgres host=localhost dbname=pet_searcher sslmode=disable password=soloyo123"
	db, err := sql.Open("postgres", connString)
	if err != nil {
		fmt.Print(err)
	}
	err = db.Ping()

	if err != nil {
		fmt.Print(err)
	}
	InitStore(&dbStore{db: db})
}
